<!-- TOC -->

- [1. 推荐系统介绍](#1-推荐系统介绍)
  - [1.1. 推荐系统架构](#11-推荐系统架构)
    - [1.1.1. 算法架构](#111-算法架构)
    - [1.1.2. 业务架构](#112-业务架构)
    - [1.1.3. 系统架构](#113-系统架构)
  - [1.2. 协同过滤算法](#12-协同过滤算法)
    - [1.2.1. 基于用户推荐 (User-Based CF)](#121-基于用户推荐-user-based-cf)
    - [1.2.2. 基于物品推荐 (Item-Based CF)](#122-基于物品推荐-item-based-cf)
    - [1.2.3. 基于模型推荐](#123-基于模型推荐)
      - [1.2.3.1. 基于矩阵分解的模型 (Low Rank Matrix Factorization)](#1231-基于矩阵分解的模型-low-rank-matrix-factorization)
  - [1.3. 推荐系统的评估](#13-推荐系统的评估)
    - [1.3.1. 常用评估指标](#131-常用评估指标)
      - [1.3.1.1. 准确性](#1311-准确性)
      - [1.3.1.2. 覆盖度](#1312-覆盖度)
      - [1.3.1.3. 多样性&新颖性&惊喜性](#1313-多样性新颖性惊喜性)
        - [1.3.1.3.1. Exploitation & Exploration 探索与利用问题](#13131-exploitation--exploration-探索与利用问题)
      - [1.3.1.4. 评估方法](#1314-评估方法)
  - [1.4. 冷启动问题](#14-冷启动问题)
    - [1.4.1. 用户冷启动](#141-用户冷启动)
    - [1.4.2. 物品冷启动](#142-物品冷启动)
    - [1.4.3. 系统冷启动](#143-系统冷启动)
  - [1.5. 基于内容的推荐](#15-基于内容的推荐)
    - [1.5.1. 基于物品的推荐和基于内容协同过滤的区别](#151-基于物品的推荐和基于内容协同过滤的区别)
  - [1.6. 案例-基于协同过滤的电影推荐](#16-案例-基于协同过滤的电影推荐)
    - [1.6.1. 数据准备](#161-数据准备)
    - [1.6.2. 加载数据集](#162-加载数据集)
    - [1.6.3. 相似度计算](#163-相似度计算)
    - [1.6.4. User-Based CF](#164-user-based-cf)
      - [1.6.4.1. 基本实现](#1641-基本实现)
    - [1.6.5. Item-Based CF](#165-item-based-cf)
    - [1.6.6. 总结](#166-总结)
- [2. 基于模型的推荐](#2-基于模型的推荐)
  - [2.1. 基于 KNN 的协同过滤推荐](#21-基于-knn-的协同过滤推荐)
  - [2.2. 基于回归模型的协同过滤推荐](#22-基于回归模型的协同过滤推荐)
    - [2.2.1. Baseline: 基准预测](#221-baseline-基准预测)
      - [2.2.1.1. 方法一: 随机梯度下降法](#2211-方法一-随机梯度下降法)
        - [2.2.1.1.1. 梯度下降法](#22111-梯度下降法)
        - [2.2.1.1.2. 随机梯度下降法](#22112-随机梯度下降法)
          - [2.2.1.1.2.1. 代码实现](#221121-代码实现)
          - [2.2.1.1.2.2. 指标评估](#221122-指标评估)
      - [2.2.1.2. 方法二：交替最小二乘法](#2212-方法二交替最小二乘法)
        - [2.2.1.2.1. 最小二乘法](#22121-最小二乘法)
        - [2.2.1.2.2. 交替最小二乘法](#22122-交替最小二乘法)
          - [2.2.1.2.2.1. 代码实现](#221221-代码实现)
  - [2.3. 基于矩阵分解的CF算法](#23-基于矩阵分解的cf算法)
    - [2.3.1. 矩阵分解发展史](#231-矩阵分解发展史)
      - [2.3.1.1. Traditinal SVD](#2311-traditinal-svd)
      - [2.3.1.2. FunkSVD (LFM)](#2312-funksvd-lfm)
      - [2.3.1.3. BiasSVD](#2313-biassvd)
      - [2.3.1.4. SVD++](#2314-svd)
    - [2.3.2. LFM(Latent factor model) 算法](#232-lfmlatent-factor-model-算法)
      - [2.3.2.1. 原理解析](#2321-原理解析)
      - [2.3.2.2. 损失函数](#2322-损失函数)
      - [2.3.2.3. 随机梯度下降法优化](#2323-随机梯度下降法优化)
      - [2.3.2.4. 代码实现](#2324-代码实现)
    - [2.3.3. BiasSVD 算法](#233-biassvd-算法)
      - [2.3.3.1. 原理分析](#2331-原理分析)
      - [2.3.3.2. 损失函数](#2332-损失函数)
      - [2.3.3.3. 随机梯度下降法优化](#2333-随机梯度下降法优化)
      - [2.3.3.4. 代码实现](#2334-代码实现)
  - [2.4. 基于内容的推荐算法 (Content-Based)](#24-基于内容的推荐算法-content-based)
    - [2.4.1. 基于内容的推荐实现步骤](#241-基于内容的推荐实现步骤)
      - [2.4.1.1. 标签从哪里来](#2411-标签从哪里来)
      - [2.4.1.2. 基于内容推荐的算法流程](#2412-基于内容推荐的算法流程)
      - [2.4.1.3. 物品冷启动处理](#2413-物品冷启动处理)
    - [2.4.2. 物品画像构建](#242-物品画像构建)
      - [2.4.2.1. 基于 TF-IDF 的特征提取技术](#2421-基于-tf-idf-的特征提取技术)
        - [2.4.2.1.1. 算法原理](#24211-算法原理)
        - [2.4.2.1.2. 数据集介绍](#24212-数据集介绍)
        - [2.4.2.1.3. 代码实现](#24213-代码实现)
          - [2.4.2.1.3.1. 加载数据集](#242131-加载数据集)
          - [2.4.2.1.3.2. 基于 TF-IDF 提取 TOP-N 关键词，构建电影画像](#242132-基于-tf-idf-提取-top-n-关键词构建电影画像)
          - [2.4.2.1.3.3. 完善画像关键词](#242133-完善画像关键词)
          - [2.4.2.1.3.4. 倒排索引](#242134-倒排索引)
    - [2.4.3. 用户画像构建 (基于物品标签建立)](#243-用户画像构建-基于物品标签建立)
    - [2.4.4. 为用户产生 TOP-N 推荐结果](#244-为用户产生-top-n-推荐结果)
    - [2.4.5. 流程总结](#245-流程总结)
    - [2.4.6. 物品冷启动问题](#246-物品冷启动问题)
      - [2.4.6.1. word2vec 原理简介](#2461-word2vec-原理简介)
        - [2.4.6.1.1. CBOW 和 SKip-Gram](#24611-cbow-和-skip-gram)
        - [2.4.6.1.2. 代码实现](#24612-代码实现)
      - [2.4.6.2. Doc2Vec](#2462-doc2vec)

<!-- /TOC -->
# 1. 推荐系统介绍
## 1.1. 推荐系统架构
### 1.1.1. 算法架构
- 推荐算法架构
  - **召回阶段 (海选阶段)**
    - 召回决定了最终推荐结果的天花板
    - 常用算法
      - 协同过滤 (基于用户、基于物品)
      - 基于内容 (根据用户行为总结出自己的偏好，根据偏好，通过文本挖掘技术找到内容上相似的商品)
      - 基于隐语义
  - **排序阶段**
    - 召回决定了最终推荐结果的天花板，排序逼近这个极限，决定了最终的推荐结果
    - CTR预估 (点击率预估，使用 LR 算法) 估计用户是否会点击某个商品，需要用户的点击数据
  - **策略调整**
    <div>
    <img src='./素材/recommend7.jpeg'>
    </div>

### 1.1.2. 业务架构
<div>
<img src='./素材/业务架构.png'>
</div>

### 1.1.3. 系统架构 
<div>
<img src='./素材/RS基础技术架构.png'>
</div>

## 1.2. 协同过滤算法
- 协同过滤的思想：物以类聚，人以群分
- **实现协调过滤推荐有以下几个步骤**
  - 找出相似的人或物品：TOP-N相似的人或物品
    - 通过计算两两相似度进行排序，即可找到 TOP-N 相似的人或物品
  - 根据相似的人或物品产生推荐结果
    - 利用TOP-N结果生成初始推荐结果，然后过滤掉用户已经有过记录的物品或明确表示不感兴趣的物品
- 协同过滤算法有以下几种
  - 基于相似度计算的算法 (memory based)
  - 基于模型的算法 (model based)
### 1.2.1. 基于用户推荐 (User-Based CF)

### 1.2.2. 基于物品推荐 (Item-Based CF)

### 1.2.3. 基于模型推荐
前两种都是简单的协同过滤算法，没有用到任何机器学习的相关的内容。基于模型的方法就是通过机器学习等算法挖掘数据中的模式，达到更好的推荐效果

#### 1.2.3.1. 基于矩阵分解的模型 (Low Rank Matrix Factorization)
- 当 user-item 矩阵比较稀疏时，直接使用 item 向量或者 user 向量去计算相似度，不太合适。
- 基于模型的方法可以**解决数据稀疏的问题**
    - [低秩矩阵分解算法](https://blog.csdn.net/weixin_45293765/article/details/109273786?spm=1001.2101.3001.6650.5&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-5-109273786-blog-72436605.experiment_layer_sa&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-5-109273786-blog-72436605.experiment_layer_sa&utm_relevant_index=10)，可以解决 user-item 矩阵稀疏的问题
      - 将大的矩阵(M×N)拆成两个小的矩阵
        - 用户矩阵 U (M×K)
        - 物品矩阵 I (K×N)
      - 大矩阵 ≈ U×I
      - 使用[交替最小二乘法(ALS)](https://www.jianshu.com/p/6bdc803913b2?utm_campaign=maleskine&utm_content=note&utm_medium=seo_notes&utm_source=recommendation)
        - 也可以使用 SGD 来求解，但是 ALS 算法并行性能比较好，适合在分布式集群中使用
  - 类似于自然语言处理的隐语义算法
- 具体过程如下
  - 优化后的用户矩阵，取出用户向量
  - 优化后的物品矩阵，取出物品向量
  - 用户向量点乘物品向量，得到用户对于该物品的评分


## 1.3. 推荐系统的评估
- 推荐系统主要实现用户、网站平台、内容提供方之间的共赢
  - 用户：满足需求、获得快乐、扩展视野
  - 平台：留住用户实现商业目标
  - 内容提供方：获取常为流量，获得互动和认可，获得收益

- 用户的反馈

||显示反馈|隐式反馈|
|:-:|:-:|:-:|
|例子|电影/书籍评分\n是否喜欢这个推荐|播放/点击、评论、下载、购买|
|准确性|高|低|
|数量|少|多|
|获取成本|高|低|

### 1.3.1. 常用评估指标
- 常用评价指标有
  - 准确性、满意度、覆盖率、多样性、新颖性、惊喜度、信任度、实时性、鲁棒性、可扩展性、商业目标、用户留存
#### 1.3.1.1. 准确性
- 准确性(从理论角度)
  - 评分预测：RMSE、MAE
  - topN推荐：召回率、精准率
- 准确性(从业务角度)
  - 上线之后看业务指标是否有提升（例如淘宝，购买率是否上升）
- 实际做推荐系统时一般是如下步骤
  - 构建模型，使用理论指标评价模型
  - 针对理论指标没问题的模型，采用一部分实际流量，使用业务指标来判定这个模型是否真的有用
  - 用实际业务指标检验后再上线投产

#### 1.3.1.2. 覆盖度
- 信息熵
  - 对于推荐来说，信息熵越大越好
  - 商品集中程度越高，说明系统熵越小
- 覆盖率
  - 覆盖率越高越好

#### 1.3.1.3. 多样性&新颖性&惊喜性
- 多样性&新颖性&惊喜性
  - 多样性：推荐列表中两两物品的不相似性。（思考相似度如何度量）
  - 新颖性：未曾关注的类别、作者；推荐结果的平均流行度
  - 惊喜性：历史不相似(惊) 但很满意 (喜)
- 实现多样性&新颖性&惊喜性，往往需要牺牲准确性
- 使用历史行为预测用户对某个物品的喜爱程度
- 系统过度强调实时性

##### 1.3.1.3.1. Exploitation & Exploration 探索与利用问题
- Exploitation (开发利用)
  - 选择现在可能最佳的方案
- Exploration (探测搜索)
  - 选择现在不确定的一些方案，但未来可能会有高收益的方案
- 在做两类决策的过程中，不断更新对所有决策的不确定性认知，优化长期的目标。这两个问题实际上是矛盾的。

- **EE问题的相关实践**
  - 兴趣扩展
    - 相似话题、搭配推荐
  - 人群算法
    - UserCF 用户聚类
  - 平衡个性化推荐和热门推荐比例
  - 随机丢弃用户行为历史
    - 阶段性丢弃
  - 随机扰动模型参数
- EE 问题可能带来的问题
  - Exploration 伤害用户体验，可能导致用户流失
  - Exploration 带来的长期收益(留存率)评估周期长，算法人员 KPI 压力大
  - 如何平衡实时兴趣和长期兴趣

#### 1.3.1.4. 评估方法
- 问卷调查
  - 成本比较高
- 离线评估
  - 只能在用户看到过的候选集上做评估，且和现上真实效果存在偏差
  - 只能评估少数指标
  - 速度快，不损害用户体验
- 在线评估
  - 灰度发布
  - A/B测试 50% 全量上线
- 实践
  - 离线评估和在线评估结合，定期做问卷调查

## 1.4. 冷启动问题
### 1.4.1. 用户冷启动
- **推荐系统冷启动的概念**
  - 用户冷启动：如何为 **<font color=red>新用户</font>** 做个性化推荐
  - 物品冷启动：如何将 **</font>新物品</font>** 推荐给用户 (协同过滤)
  - 系统冷启动：用户冷启动 + 物品冷启动
  - 本质是推荐系统依赖历史数据，没有历史数据无法预测用户偏好
- **用户冷启动**
  - 1. 收集用户特征（用户画像，打标签；根据用户标签做人群聚类，用以做相似推荐）
    - 用户注册信息：性别、年龄、地域
    - 设备信息：定位、手机号码、app列表
    - 社交信息、推广素材、安装来源

<div align=center>
<img src='./素材/用户冷启动.png'>
</div>

- 2. 引导用户填写兴趣
- 3. 使用其它站点的行为数据，例如腾讯视频&QQ音乐、今日头条&抖音 （需要有对应的生态）
- 4. 新老用户推荐策略的差异
  - 新用户在冷启动阶段更倾向于热门排行榜，老用户会更加需要长尾推荐
    - [长尾问题](https://blog.csdn.net/m0_52122378/article/details/118331831?utm_medium=distribute.pc_feed_404.none-task-blog-2~default~BlogCommendFromBaidu~Rate-5-118331831-blog-null.pc_404_mixedpudn&depth_1-utm_source=distribute.pc_feed_404.none-task-blog-2~default~BlogCommendFromBaidu~Rate-5-118331831-blog-null.pc_404_mixedpud)
      - 可以看成是推荐系统倾向于推荐热门商品，而忽略了非热门物品
  - Explore Exploit力度
    - 给新用户的策略保守一点
  - 使用单独的特征和模型预估

### 1.4.2. 物品冷启动
- **物品冷启动**
  - 给物品打标签
  - 利用物品的内容信息，将新物品先投放给曾经喜欢过和它相似的其他物品的用户
    - 基于内容的推荐

### 1.4.3. 系统冷启动
- 系统冷启动
  - 物品冷启动 + 用户冷启动
  - 系统早期：缺少用户行为的数据
    - 采用基于内容的推荐
  - 随着用户行为积累越来越多
    - 基于内容的推荐逐渐过渡到协同过滤
  - 基于内容的推荐和协同过滤推荐结果都计算出来，加权求和得到最终推荐结果


## 1.5. 基于内容的推荐
- 基于内容的推荐
  - 需要尽可能给物品打上标签
    - 系统自己提取
      - 业务数据库中提取，例如商品描述、标题、分类
    - 用户填写
    - 然后利用进行分词，利用 NLP 计算词的权重
      - 例如 TF-IDF、TextRank
  - 利用标签的文字。转换成词向量
    - word2vec
      - 用向量表示语义
      - 如果两个词相似度较高，则表示它们比较接近
  - 利用词向量构建一个物品的向量
    - 一个物品有 N 个关键词，每个关键词又对应着一个词向量
    - 物品向量 = (Σ权重×词向量)/N
  - 通过物品向量来计算相似度

- 基于内容的推荐
  - 不仅仅可以应用在电商场景，也可以用在短视频、音乐
  - 只要能够提取出标签的场景，都可以使用

### 1.5.1. 基于物品的推荐和基于内容协同过滤的区别
- 基于内容的推荐(Content-Based CF)
  - 通过词向量来构建物品向量，然后来计算物品的相似度
    - **不依赖于用户评分矩阵**
  - 和吴恩达的例子稍有不同
- Item-Based CF
  - 利用 user-Item matrix 得到物品向量，根据物品向量计算相似度
  - 依赖于用户评分矩阵
- 基于物品的推荐和基于内容协同过滤的区别
  - 物品向量的构建来源不一样
    - 基于内容的推荐
      - 物品向量来自于文本 (物品的描述信息、系统填写的标签、用户填写的标签)
      - 不依赖于用户行为数据
    - 基于物品的协同过滤
      - 物品向量来自于用户对物品的评分矩阵
        - 要求有用户的行为数据

## 1.6. 案例-基于协同过滤的电影推荐
- 前面介绍了协同过滤的推荐算法，主要有两种实现方案
  - User-Based CF
  - Item-Based CF
- 下面采用真实数据集来进行实现

### 1.6.1. 数据准备
- 数据集下载
  - 有关数据集的介绍，来自 https://blog.csdn.net/weixin_42727538/article/details/124364700
  - 我们这里采用 20M 的数据。可以从[这里](./素材/数据/ml-20m.zip)下载
    - 里面的 ratings.csv 就是用户-电影评分数据
- 数据集描述
  - This dataset (ml-20m) describes 5-star rating and free-text tagging activity from [MovieLens](http://movielens.org), a movie recommendation service. It contains 20000263 ratings and 465564 tag applications across 27278 movies. These data were created by 138493 users between January 09, 1995 and March 31, 2015. This dataset was generated on March 31, 2015, and updated on October 17, 2016 to update links.csv and add genome-* files.
  - Users were selected at random for inclusion. All selected users had rated at least 20 movies. No demographic information is included. Each user is represented by an id, and no other information is provided.
  - The data are contained in six files, `genome-scores.csv`, `genome-tags.csv`, `links.csv`, `movies.csv`, `ratings.csv` and `tags.csv`. More details about the contents and use of all these files follows.
  - This and other GroupLens data sets are publicly available for download at <http://grouplens.org/datasets/>.

### 1.6.2. 加载数据集
>```python
>import pandas as pd
>import numpy as np
>
>DATA_ROOT_PATH = './ml-20m'
>DATA_PATH = DATA_ROOT_PATH + '/ratings.csv'
>cache_dir = './cache'
>data = pd.read_csv(DATA_PATH)
>```
- 数据结果如下

|          | userId	|movieId|	rating	|timestamp  |
|-:|-:|-:|-:|-:|    
|    0	  |  1	    |2	    |3.5	    |1112486027 |
|    1	  |  1	    |29	    |3.5	    |1112484676 |
|    2	  |  1	    |32	    |3.5	    |1112484819 |
|    3	  |  1	    |47	    |3.5	    |1112484727 |
|    4	  |  1	    |50	    |3.5	    |1112484580 |
|    ...	|  ...	|...	|...	    |...        |
|20000258|	138493	|68954	|4.5	    |1258126920 |
|20000259|	138493	|69526	|4.5	    |1259865108 |
|20000260|	138493	|69644	|3.0	    |1260209457 |
|20000261|	138493	|70286	|5.0	    |1258126944 |
|20000262|	138493	|71619	|2.5	    |1255811136 |
20000263 rows × 4 columns

实际上在我们生产开发过程中不可能获取到这么整齐的数据，这份数据明显是经过 ETL 之后的数据。

- 封装一个数据加载函数
  - 其返回结果是一个 user-item 矩阵
>```python
>def load_data(data_path):
>    '''
>    加载数据
>    :param data_path: 数据集路径
>    :param cache_path: 数据集缓存路径
>    :return: 用户-物品评分矩阵
>    '''
>    # 数据集缓存地址
>    cache_path = os.path.join(cache_dir,'ratings_matrix.cache')
>    
>    print('开始读取数据集...')
>    if os.path.exists(cache_path):
>        print("加载缓存中...")
>        ratings_matrix = pd.read_pickle(cache_path)
>        print('从缓存加载数据集完毕')
>    else:
>        print('加载新数据...')
>        # 设置需要加载的数据字段类型
>        dtype = {'userId':np.int64,'movieId':np.int64,'rating':np.float64}
>        # 加载数据，这里只需要三列数据，分别是 userId、mobvieId、rating
>        ratings = pd.read_csv(data_path,dtype=dtype,usecols=range(3)).iloc[:200*10000,:] # 只取前三个列, 只读取前200W条数据 (数据太大了，笔记本吃不消)
>        # 透视表
>        # 将 movieId 转换为列名，转换成一个完整的 user-movie 平稳矩阵
>        ratings_matrix = ratings.pivot_table(index=['userId'],columns=['movieId'],values='rating')
>        # 将数据存入缓存
>        os.makedirs(cache_dir,exist_ok=True) # os.makedirs() 可以递归创建目录
>        ratings_matrix.to_pickle(cache_path)
>        print('数据集加载完毕')
>    return ratings_matrix
> ```
- 查看返回结果
  - ratings_maxtrix = load_data(DATA_PATH)

<div align=center>
<img src='./素材/uer-movie-matrix.png'>
</div>

- 有关 pivot_table，详见 [pandas 文档](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.pivot_table.html?highlight=pivot_table#pandas.DataFrame.pivot_table)

### 1.6.3. 相似度计算
- 相似度计算
  - 计算用户-用户之间的相似度
  - 计算物品-物品的相速度
>```python
># 计算相似度
>def compute_pearson_similarity(ratings_matrix,based='user'):
>    '''
>    基于皮尔逊相关系数计算相似度
>    :param ratings_matrix: 用户-物品评分矩阵
>    :param based: 基于谁计算相似度，默认基于用户计算相似度
>    :return similarity 矩阵
>    '''
>    # 用户相似度
>    if based == 'user':
>        similarity = ratings_matrix.T.corr()
>    elif based == 'item':
>        similarity = ratings_matrix.corr()
>    else:
>        raise Exception("undefined 'based' value: %s"%based)
>    print('相似度计算完毕')
>    return similarity
> ```
- 计算用户相似度
  - compute_pearson_similarity(ratings_matrix)

<div align=center>
<img src='./素材/用户相似度.png'>
</div>

- 计算物品相似度
<div align=center>
<img src='./素材/物品相似度.png'>
</div>

- 注意为什么计算 uer-item 矩阵相似度时，用户相似度用的是 ratings_matrix.T.corr() 而不是 ratings_matrix.corr()？
  - 这和 pandas 的 corr() 函数有关系
    - 该函数的功能是 [Compute pairwise correlation of columns, excluding NA/null values.](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.corr.html?highlight=corr#pandas.DataFrame.corr)，由于 userId 是 row, 因此需要先转置
- 经过上面结果也可以看到，计算出来的相似度矩阵异常稀疏
  - 不适合用这种方法来求解
- 注意：上述案例中由于数据量较大，只是选取了一部分数据进行计算
  - 以上实现，仅仅用于实验阶段
  - 生产实际中数据量过大，pandas 无法支撑大批数据计算
  - 通常使用 spark、mapreduce 等分布式框架代替
    - 但是实现思想是一致的。

### 1.6.4. User-Based CF
- user-Based CF 评分预测公式如下

$$pred(u,i) = \hat{\tau}_{ui} = \frac{\Sigma_{v∈U}sim(u,v)*\tau_{vi}}{\Sigma_{v∈U}|sim(u,v)|}$$


- 实现思路
  - 找到给定 uid 用户的相似用户
  - 从 uid 相似近邻用户中挑选出对 iid 物品有评分的用户 (需要排除用户本身)
  - 根据公式计算
    - 分子为 uid 和近邻用户的相似度 × 对iid的评分，然后求和
    - 分母为 uid 和近邻用户的相似度绝对值之和

#### 1.6.4.1. 基本实现
- 预测给定用户 uid 对于给定物品 iid 的评分
>```python
>def predict(uid,iid,ratings_matrix,user_similarity):
>    '''
>    预测给定用户对给定物品的评分值
>    :param uid: 用户ID
>    :param iid: 物品ID
>    :param ratings_matrix: 用户-物品评分矩阵
>    :param user_similarity; 用户之间的两两相似度
>    :return: 预测的评分值
>    '''
>    print('开始预测用户 <%d> 对电影 <%d> 的评分'%(uid,iid))
>    # 获取用户uid的相似用户
>    sim_users = user_similarity[uid].drop(labels=[uid],axis=0).dropna()
>    # 相似用户筛选规则：筛选出正相关的用户
>    sim_users = sim_users.where(sim_users>0).dropna()
>    if sim_users.empty:
>        raise Exception('用户 <%d> 没有相似用户'%uid)
>    
>    # 从 uid 近邻相似用户中筛选对 iid 有评分的用户
>    id_sim_users = sim_users.index.unique() & ratings_matrix.loc[:,iid].dropna().index.unique()
>    finally_sim_users = sim_users.loc[id_sim_users]
>    
>    # 开始预测评分
>    sum_up = 0
>    sum_down = 0
>    for sim_user_id,sim in finally_sim_users.iteritems():
>        sum_up += sim*ratings_matrix.loc[sim_user_id,iid] 
>        sum_down += sim
>    return round(sum_up/sum_down,2)
> ```

- 预测用户 uid 对于所有物品的评分
>```python
>def _predict_all(uid,item_ids,ratings_matrix,user_similarity):
>    '''
>    预测全部评分
>    :param uid: 用户id
>    :param item_ids: 物品id列表
>    :param ratings_matrux: 用户-物品评分矩阵
>    :param user_similarity: 用户之间相似度矩阵
>    return 生成器，逐个返回预测评分
>    '''
>    # 逐个预测
>    for iid in item_ids:
>        try:
>            rating = predict(uid,iid,ratings_matrix,user_similarity)
>        except Exception as e:
>            print(e)
>        else:
>            yield uid,iid,rating
>```
- 添加过滤规则
>```python
>def predict_all(uid,ratings_matrix, user_similarity,filter_rule=None):
>    '''
>    预测全部评分
>    :param uid: 用户id
>    :param ratings_matrix: 用户-物品评分矩阵
>    :param user_similar: 用户两两之间的相似度
>    :param filter_rule: 过滤规则，只能是四选一，否则将抛出异常:"unhot","rated",["unhot","rated"],None
>    :return 生成器，逐个返回用户对电影的评分
>    '''
>    if not filter_rule:
>        item_ids = ratings_matrix.columns
>    elif isinstance(filter_rule,str) and filter_rule == 'unhot':
>        # 过滤非热门电影
>        # 统计每部电影的评分数
>        count_ = ratings_matrix.count()
>        # 过滤出评分数高于 10 的电影，作为热门电影
>        item_ids = count_.where(count_>10).dropna().index
>    elif isinstance(filter_rule,str) and filter_rule == 'rated':
>        # 过滤该用户评分过的电影
>        user_ratings = ratings_matrix.loc[uid,:]
>        # 评分为 1-5 是有效评分，其余是无效评分
>        idx = user_ratings<6
>        item_ids = idx.where(idx==False).dropna().index
>    elif isinstance(filter_rule,list) and set(filter_rule) == set(['unhot','rated']):
>        # 过滤非热门和用户评分过的电影
>        count_ = ratings_matrix.count()
>        # 过滤出评分数高于 10 的电影，作为热门电影
>        index_ids1 = count_.where(count_>10).dropna().index
>        
>        user_ratings = ratings_matrix.loc[uid,:]
>        # 评分为 1-5 是有效评分，其余是无效评分
>        idx = user_ratings<6
>        index_ids2 = idx.where(idx==False).dropna().index
>        
>        # 两者交集
>        item_ids = index_ids1.intersection(index_ids2)
>    else:
>        raise Exception('输入过滤参数错误')
>    yield from _predict_all(uid,item_ids,ratings_matrix,user_similarity)
> ```
- 根据预测评分为用户进行 TOP-N 推荐
>```python
># 根据预测评分排序并输出 top-N
>def top_k_recomand(k,uid,ratings_matrix,based = 'user',filer_rule=None):
>    # 计算用户相似度
>    user_similarity = compute_pearson_similarity(ratings_matrix,based=based)
>    # 计算用户 uid 对于物品的评分
>    ratings = predict_all(uid,ratings_matrix,user_similarity,filer_rule)
>    res = sorted(ratings,key=lambda x:x[2],reverse=True)
>    return res[:k]
> ```

### 1.6.5. Item-Based CF
- Item-Based CF 的评分预测公式如下，考虑了用户自身的平均打分因素，结合预测物品与相似物品的加权平均相似度

$$pred(u,i) = \hat{\tau}_{ui} = \frac{\Sigma_{j∈I_{rated}}sim(i,j)*\tau_{uj}}{\Sigma_{j∈I_{rated}}sim(i,j)}$$

- 实现思路
  - 找到物品 iid 的近邻物品
  - 找到物品 iid 的近邻物品中有评分的物品 (排除物品 iid 本身)
  - 按照公式计算
    - 分子为物品 iid 与其近邻有评分物品的相似度 × 用户 uid 对近邻物品的评分
    - 分母为物品 iid 与其近邻有评分物品的相似度之和

- 给定用户对于单个给定物品的评分
>```python
>def predict_item_based(uid,iid,ratings_matrix,item_similarity):
>     '''
>    预测给定用户对给定物品的评分值
>    :param uid: 用户ID
>    :param iid: 物品ID
>    :param ratings_matrix: 用户-物品评分矩阵
>    :param suer_similar; 用户之间的两两相似度
>    :return: 预测的评分值
>    '''
>    print('开始预测用户 <%d> 对电影 <%d> 的评分'%(uid,iid))
>    # 获取物品 iid 相似的物品
>    sim_items= item_similarity[iid].drop(labels=[iid],axis=0).dropna()
>    # 相似物品筛选规则：筛选出正相关的物品
>    sim_items = sim_items.where(sim_items>0).dropna()
>    if sim_items.empty:
>        raise Exception('物品 <%d> 没有相似物品'%iid)
>    
>    # 从 iid 近邻相似物品中筛选 uid 有评分的物品
>    id_sim_items = sim_items.index.unique() & ratings_matrix.loc[uid,:].dropna().index.unique()
>    finally_sim_items = sim_items.loc[id_sim_items]
>    
>    # 开始预测评分
>    sum_up = 0
>    sum_down = 0
>    for sim_item_id,sim in finally_sim_items.iteritems():
>        sum_up += sim*ratings_matrix.loc[uid,sim_item_id] 
>        sum_down += sim
>    return round(sum_up/sum_down,2)
> ```

- 其余同 User-Based CF

### 1.6.6. 总结
上述 User-Based CF 和 Item-Based CF 并未用到 KNN 的思想，在筛选近邻用户/物品时，仅做了正相关的筛选。如果要考虑 KNN 的思想，以 User-Based CF 为例，则只需要在 predict() 函数里面筛选近邻用户时，finally_sim_users = sim_users.loc[id_sim_users] 作一个排序，截取 TOP-N 个


# 2. 基于模型的推荐
- Model Based CF 算法可以大致分为以下几类
  - 基于分类算法、回归算法、聚类算法
  - 基于矩阵分解的推荐
  - 基于神经网络
  - 基于图模型
- 将要学习以下几种使用比较多的方案
  - 基于 K 最近邻的协同过滤推荐
  - 基于回归模型的协同过滤推荐
  - 基于矩阵分解的协同过滤推荐

## 2.1. 基于 KNN 的协同过滤推荐
基于 KNN 的协同过滤推荐，本质上就是 MemoryBased CF, 只不过在选择近邻的时候，加上了 K 最近邻的限制
>```python
>class CollaborativeFiltering(object):
>    based = None
>    
>    def __init__(self,k=40,rules=None,use_cache=False,standard=None):
>        '''
>        :param k: 取 k 个最近邻来进行预测
>        :param rules: 过滤规则，四选一，否则抛出异常.'unhot','rated'.['unhot'.'rated'],None
>        :param use_cache: 相似度计算结果是否开启缓存
>        :param standard: 评分标准化方法，None标识不适用，mean标识均值中心化，zscore表示z-score标准化
>        '''
>        self.k = k
>        self.rules = rules
>        self.use_cache = use_cache
>        self.standard = standard
> ```
- 修改所有选取近邻的地方的代码，根据相似度截取前 K 个最近邻
>```python
> # 对于 Series , 值排序方法是 sort_values()
>sim_users = user_similarity[uid].drop(labels=[uid],axis=0).dropna().sort_values(ascending=False)[:self.k]
>
>sim_items = item_similarrity[iid].drop(labels=[iid],axis=0).dropna().sort_values(ascendig=False)[:self.k]
>```
- 当原始数据较少时，使用 KNN 方法会比纯粹使用 MemoryBased CF 效果差一些

## 2.2. 基于回归模型的协同过滤推荐
如果我们将评分看作是一个连续的值，而不是离散的值，那么可以借助线性回归的思想来预测目标用户对某物品的评分，其中一种实现策略被称为时 Baseline (基准思想)

### 2.2.1. Baseline: 基准预测
- **Baseline 设计思想基于以下假设**
  - 有些用户的评分普遍高于其他用户，有些用户的评分普遍低于其他用户。
    - 比如有些用户天生愿意给别人好评，心慈手软
    - 有些用户比较刻薄，总是评分不超过 3 分 (5分满分)
  - 一些物品的评分普遍高于其他物品，一些物品的评分普遍低于其他物品
    - 比如一些物品一被生产就决定了它的地位，有的比较受欢迎，有的则被嫌弃
  - 用户或者物品普遍高于或者低于平均值的差值，就叫做偏置 (bias)
- **Baseline 目标**
  - 找出每个用户普遍高于或低于他人的偏置值 $b_u$
  - 找出每件物品普遍高于或低于其他物品的偏置值 $b_i$
  - 我们的目标也就是寻找最优的 $b_u$ 和 $b_i$
- **使用 Baseline 算法思想的步骤如下**
  - 计算所有电影的平均评分 $\mu$ (即全局平均评分)
  - 计算 **每个用户** 评分与平均分 $\mu$ 的偏置值 $b_u$
  - 计算 **每部电影** 所接受的评分与平均分 $\mu$ 的偏置值 $b_i$
  - 预测用户对电影的评分 $\hat{\tau_{ui}} = b_{ui} = \mu + b_u +b_i$
- 构建损失函数如下

$Cost = \Sigma_{u,i∈R}{(\tau_{ui} - \hat{\tau_{ui})^2}} =\Sigma_{u,i∈R}{(\tau_{ui} - \mu - b_u - b_i)^2} $

- 加入 L2 正则项

$J(\theta) = Cost =\Sigma_{u,i∈R}{(\tau_{ui} - \mu - b_u - b_i)^2} + \lambda(\underset{u}{\Sigma}{b_u^2}+\underset{i}{\Sigma}{b_i^2})$

#### 2.2.1.1. 方法一: 随机梯度下降法
##### 2.2.1.1.1. 梯度下降法
梯度下降法参数更新公式：$\theta_j :=\theta_j - \alpha\frac{\partial}{\partial\theta_j}J(\theta)$
得到 $b_u$ 的更新公式为 $b_u := b_u - \alpha*(-\underset{u∈R}{\Sigma}(r_{ui}-\mu-b_u-b_i)+\lambda*b_u)$

$b_i := b_i - \alpha*(-\underset{u∈R}{\Sigma}(r_{ui}-\mu-b_u-b_i)+\lambda*b_i)$

##### 2.2.1.1.2. 随机梯度下降法
- 每一个样本都对参数进行一次更新
- 单样本损失函数：

$error = r_{ui} - \hat{r_{ui}} = r_{ui} - \mu - b_u - b_i$

- 参数更新

$b_u := b_u + \alpha*(error - \lambda*b_u)$

$b_i := b_i + \alpha*(error - \lambda*b_i)$

###### 2.2.1.1.2.1. 代码实现
>```python
>import pandas as pd
>import numpy as np
>
>class BaseLineCFBySGD(object):
>    def __init__(self, number_epochs,alpha,reg, columns=['uid','iid','ratings']):
>        # 梯度下降最高迭代次数
>        self.number_epochs = number_epochs
>        # 学习率
>        self.alpha = alpha
>        # 正则化系数
>        self.reg = reg
>        # 数据集中 user-item-rating 字段的名称
>        self.columns = columns
>    
>    def fit(self,dataset):
>        '''
>        :param dataset: uid, iid, rating
>        :return:
>        '''
>        self.dataset = dataset[self.columns]
>        # 用户评分数据
>        self.user_ratings = dataset.groupby(self.columns[0]).agg([list])[self.columns[1:]]
>        # 物品评分矩阵
>        self.item_ratings = dataset.groupby(self.columns[1]).agg([list])[[self.columns[0],self.columns[2]]]
>        # 计算全局平均分
>        self.global_mean = self.dataset[self.columns[2]].mean()
>        # 调用 sgd 方法训练模型参数
>        self.bu,self.bi = self.sgd()
>        
>    def sgd(self):
>        '''
>        利用随机梯度下降，优化 bu,bi 的值
>        :return: bu , bi
>        '''
>        # 初始化参数 bu、bi，全部设置为 0 
>        bu = dict(zip(self.users_ratings.index,np.zeros(len(self.user_ratings))))
>        bi = dict(zip(self.item_ratings.index,np.zeros(len(self.item_ratings))))
>        
>        # 循环迭代优化
>        for i in range(self.number_epochs):
>            print('iter %d'% i)
>            for uid, iid, real_rating in self.dataset.itertuples(index=false):
>                error = real_rating - (self.global_mean + bu[uid]+bi[iid])
>                
>                bu[uid] += self.alpha * (error - self.reg*bu[uid])
>                bi[iid] += self.alpha * (error - self.reg*bi[iid])
>        return bu,bi
>    
>    def predict(self,uid,iid):
>        if iid not in self.item_ratings.index:
>           raise Exception('无法预测<{uid}>对于<{iid}>的数据，因为训练集中缺少<{iid}>的数据'.format(uid=uid,iid=iid))
>        predict_rating = self.global_mean + self.bu[uid] + self.bi[iid]
>        return predict_rating
>
>    def test(self,testset):
>        '''预测测试集数据'''
>        for uid,iid,real_rating in tests.itertuples(index=False):
>            try:
>                pred_rating = self.predict(uid,iid)
>            except Exception as e:
>                print(e)
>            else:
>                yield uid,iid,real_rating,pred_rating
> ```

###### 2.2.1.1.2.2. 指标评估
- 切分数据集
>```python
>def data_split(data_path, x=0.8, random=False):
>    '''
>    切分数据集，这里为了保证用户数量保持不变，将每个用户的评分数据按比例进行拆分
>    :param data_path: 数据集路径
>    :param x: 训练集比例，如 x= 0.8，则测试集为 0.2
>    :param random: 是否随机切分，默认 False
>    :return : 用户-物品评分矩阵
>    '''
>    print("开始切分数据集...")
>    # 设置需要加载的数据集的类型
>    dtype = {'userId':np.int32,'movieId':np.int32,'rating':np.float32}
>    # 加载数据，我们只用前三列数据，分别是用户id、电影id、用户对电影的评分
>    ratings = pd.read_csv(data_path,dtype=dtype,usecol=range(3))
>    
>    testset_index = []
>    # 为了保证每个用户在测试集和训练集都有数据，因此按userId聚合
>    for uid in ratings.groupby('userId').any().index:
>        user_rating_data = ratings.where(ratings['userId']==uid).dropna() # 把每个 userId 的数据拿出来，按比例切分
>        if random:
>            # 因为不可卞类型不能被 shuffle() 方法作用，所以需要强行转换为列表
>            index = list(user_rating_data.index)
>            np.random.shuffle(index) # 打乱列表
>            _index = round(len(user_rating_data)*x)
>            testset_index+=list(index[_index:])
>        else:
>            # 将两个用户的 x 比例的数据作为训练集，剩余的作为测试集
>            index = round(len(user_rating_data)*x)
>            testset_index += list(user_rating_data.index.values[index:])
>    
>    testset = ratings.loc[testset_index]
>    trainset = ratings.drop(testset_index)    
>    print('数据集切分完成...')
>    return trainset,testset
> ```
- 指标评估
>```python
>def accuracy(predict_results,method = 'all'):
>    '''
>    准确性指标计算方法
>    :param predict_results: 预测结果，类型为容器，每个元素是一个包含 uid,iid, real_rating, pred_rating 的序列
>    :param method: 指标方法，类型为字符串，rmse 或者 mae，否则返回两者 rmse 和 mae
>    :return :
>    '''
>    def rmse(predict_results):
>        '''
>        rmse评估指标
>        :param predict_results
>        :return : rmse
>        '''
>        length = 0
>        _rmse_sum = 0
>        for uid, iid, real_rating, pred_rating in predict_results:
>            length += 1
>            _rmse_sum += (pred_rating - real_rating)**2
>        return round(np.sqrt(_rmse_sum/length),4)
>    
>    def mae(predict_results):
>        length = 0
>        _mae_sum = 0
>        for uid, iid, real_rating, pred_rating in predict_results:
>            length += 1
>            _mae_sum += abs(pred_rating - real_rating)
>        return round(np.sqrt(_mae_sum/length),4)
>        
>    if method=='rmse':
>        return rmse(predict_results)
>    elif method == 'mae':
>        return mae(predict_results)
>    elif method == 'all':
>        return rmse(predict_results),mae(predict_results)
>    else:
>        raise Exception('传入参数method有误')
> ```

#### 2.2.1.2. 方法二：交替最小二乘法
##### 2.2.1.2.1. 最小二乘法
- 最小二乘法思想：对损失函数求偏导，然后使骗到为0
- 同样，损失函数为

$J(\theta) = \underset{u,i∈R}{\Sigma}(\tau_{ui}-\mu-b_u-b_i)^2+\lambda*(\underset{u}{\Sigma}b_u^2+\underset{i}{\Sigma}b_i^2)$

- 对损失函数求偏导

$$\frac{\partial}{\partial{b_u}}f(b_u,b_i)=-2\underset{u,i∈R}{\Sigma}(\tau_{ui}-\mu-b_u-b_i)+2\lambda*b_u$$

- 令偏导为 0 ，可得

$$\underset{u,i∈R}{\Sigma}(\tau_{ui}-\mu-b_u-b_i) = \lambda*b_u$$

$$\underset{u,i∈R}{\Sigma}(\tau_{ui}-\mu-b_i) = \underset{u,i∈R}{\Sigma}b_u+\lambda*b_u$$

- 为了简化公式，这里令 $\underset{u,i∈R}{\Sigma}b_u≈|R(u)|*b_u$ , 即直接假设每一项偏置项都相等，则可得：

$$b_u:=\frac{\underset{u,i∈R}{\Sigma}(\tau_{ui}-\mu-b_i)}{\lambda_1+|R(u)|}$$

- 其中 $|R(u)|$ 表示用户 u 的有评分的数量
- 同理可得

$$b_i:=\frac{\underset{u,i∈R}{\Sigma}(\tau_{ui}-\mu-b_u)}{\lambda_2+|R(i)|}$$

- 其中 $|R(i)|$ 表示物品 i 的收到的评分数量
- 因为 $b_u$ 和 $b_i$ 分别属于用户和物品的偏置，因此他们的正则参数可以分别设置两个独立的参数

##### 2.2.1.2.2. 交替最小二乘法
- 上述最小二乘法的推导，我们最终得到了 $b_u$ 和 $b_i$ 的表达式
  - 但是他们互相包含对方，我们可以使用一种叫 **交替最小二乘法 (ALS)** 的方法来计算他们的值
    - 计算其中一项时，先固定其他未知参数，即看其他参数为已知
    - 如求 $b_u$ 时，将 $b_i$ 看成已知；求 $b_i$ 时，将 $b_u$ 看成已知；如此反复交替，不断更新两者的值，求得最终结果

###### 2.2.1.2.2.1. 代码实现
- 其余部分同梯度下降法几乎都一样
  - 只有迭代计算部分不同
>```python
>    def als(self):
>        '''
>        利用交替二乘法优化bu、bi的值
>        :return: bu、bi
>        '''
>        # 初始化bu、bi的值，全部设置为0
>        bu = dict(zip(self.users_ratings.index,np.zeros(len(self.user_ratings))))
>        bi = dict(zip(self.item_ratings.index,np.zeros(len(self.item_ratings))))
>        
>        for i in range(self.number_epochs):
>            print('iter %d'%i)
>            for iid,uid,ratings in self.item_ratings.itertuples(index=True): # 这里得到的是 iid, [uids...],[ratings....]的结构
>                __sum = 0
>                for uid,rating in zip(uids,ratings):
>                    __sum += rating - self.global_mean - bu[uid]
>                bi[iid] = __sum / (self.reg_bi + len(uids))
>                
>            for uid,iids,ratings in self.user_ratings.itertuples(index=True):
>                __sum = 0
>                for iid, rating in zip(iids,ratings):
>                    __sum += rating - self.global_mean - bi[iid]
>                bu[uid] = __sum / (self.reg_mu + len(iids))
>        return bu,bi
> ```

## 2.3. 基于矩阵分解的CF算法
### 2.3.1. 矩阵分解发展史
#### 2.3.1.1. Traditinal SVD
- 通常SVD矩阵分解指的是SVD（奇异值）分解技术，在这我们姑且将其命名为Traditional SVD（传统并经典）其公式如下

$$M_{m×n} = U_{m×k}\Sigma_{k×k}V_{k×n}^T$$

- Traditional SVD分解的形式为3个矩阵相乘，中间矩阵为奇异值矩阵。
  - 如果想运用SVD分解的话，有一个前提是要求矩阵是稠密的，即矩阵里的元素要非空，否则就不能运用SVD分解.
- 很显然我们的数据其实绝大多数情况下都是稀疏的，
  - 因此如果要使用Traditional SVD，一般的做法是先用均值或者其他统计学方法来填充矩阵，然后再运用Traditional SVD分解降维
  - 但这样做明显对数据的原始性造成一定影响。

#### 2.3.1.2. FunkSVD (LFM)
- 刚才提到的Traditional SVD首先需要填充矩阵，然后再进行分解降维，同时存在计算复杂度高的问题，因为要分解成3个矩阵，
  - 所以后来提出了Funk SVD的方法，它不在将矩阵分解为3个矩阵
  - 而是分解为2个 **用户-隐含特征**，**项目-隐含特征** 的矩阵，Funk SVD也被称为最原始的LFM模型
- 加上正则项后，其最后求解表达式为

$$\underset{q*,p*}{min}\underset{u,i∈R}{\Sigma}(r_{ui}-q_i^Tp_u)^2+\lambda(||q_i||^2+||p_u||^2)$$

- 可以通过梯度下降法进行求解

#### 2.3.1.3. BiasSVD
- 在FunkSVD提出来之后，出现了很多变形版本
  - 其中一个相对成功的方法是BiasSVD，顾名思义，即带有偏置项的SVD分解

$$\underset{q_i,p_u}{argmin}\underset{u,i∈R}{\Sigma}(r_{ui}-q_i^Tp_u-\mu-b_i-b_u)^2+\lambda(||q_i||^2+||p_u||^2+||b_i||^2+||b_u||^2)$$

- 它基于的假设和Baseline基准预测是一样的，但这里将Baseline的偏置引入到了矩阵分解中

#### 2.3.1.4. SVD++
- 人们后来又提出了改进的BiasSVD，被称为SVD++
  - 该算法是在BiasSVD的基础上添加了用户的隐式反馈信息：

$$
\underset{q_i,p_u}{argmin}\underset{u,i∈R}{\Sigma}(r_{ui}-q_i^Tp_u-\mu-b_i-b_u-q_u^T|N(i)|^{-1/2}\underset{s∈N(i)}{\Sigma}y_s)^2+\lambda(||q_i||^2+||p_u||^2+||b_i||^2+||b_u||^2+\underset{s∈N(i)}{\Sigma}||y_s||_2^2)
$$

- 显式反馈指的是用户的评分这样的行为，隐式反馈指用户的浏览记录、购买记录、收听记录等
- SVD++ 是基于这样的假设：
  - 在 BiasSVD 的基础上，认为用户对于项目的历史浏览记录、购买记录、收听记录等可以侧面反映用户的偏好

### 2.3.2. LFM(Latent factor model) 算法
- LFM 算法，也就是前面提到的 FunkSVD 算法
#### 2.3.2.1. 原理解析
- LFM (Latent Factor Model) 隐语义模型核心思想是通过隐含特征联系
<div align=center>
<img src='./素材/LFM原理.png'>
</div>

- 其中
  - P 矩阵是 User-LF 矩阵，即用户-隐含特征矩阵
    - 图中 LF 有三个，表示总共有三个隐含特征
  - Q 矩阵是 LF-Item 矩阵，即隐含特征-物品矩阵
  - R 矩阵是 User-Item 矩阵，由 P*Q 得来
- 该算法可以处理稀疏评分的情况

#### 2.3.2.2. 损失函数

同样对于评分预测我们利用平方差来构建损失函数：

$$
\begin{split}
Cost &= \sum_{u,i\in R} (r_{ui}-\hat{r}_{ui})^2
\\&=\sum_{u,i\in R} (r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})^2
\end{split}
$$

加入L2正则化：

$$
Cost = \sum_{u,i\in R} (r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})^2 + \lambda(\sum_U{p_{uk}}^2+\sum_I{q_{ik}}^2)
$$

对损失函数求偏导：

$$
\begin{split}
\cfrac {\partial}{\partial p_{uk}}Cost &= \cfrac {\partial}{\partial p_{uk}}[\sum_{u,i\in R} (r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})^2 + \lambda(\sum_U{p_{uk}}^2+\sum_I{q_{ik}}^2)]
\\&=2\sum_{u,i\in R} (r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})(-q_{ik}) + 2\lambda p_{uk}
\\\\
\cfrac {\partial}{\partial q_{ik}}Cost &= \cfrac {\partial}{\partial q_{ik}}[\sum_{u,i\in R} (r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})^2 + \lambda(\sum_U{p_{uk}}^2+\sum_I{q_{ik}}^2)]
\\&=2\sum_{u,i\in R} (r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})(-p_{uk}) + 2\lambda q_{ik}
\end{split}
$$

#### 2.3.2.3. 随机梯度下降法优化

梯度下降更新参数$p_{uk}​$：
$$
\begin{split}
p_{uk}&:=p_{uk} - \alpha\cfrac {\partial}{\partial p_{uk}}Cost
\\&:=p_{uk}-\alpha [2\sum_{u,i\in R} (r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})(-q_{ik}) + 2\lambda p_{uk}]
\\&:=p_{uk}+\alpha [\sum_{u,i\in R} (r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})q_{ik} - \lambda p_{uk}]
\end{split}
$$
 同理：
$$
\begin{split}
q_{ik}&:=q_{ik} + \alpha[\sum_{u,i\in R} (r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})p_{uk} - \lambda q_{ik}]
\end{split}
$$
**随机梯度下降：** (随机梯度下降：每个样本更新一次参数) 向量乘法 每一个分量相乘 求和。
$$
\begin{split}
&p_{uk}:=p_{uk}+\alpha [(r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})q_{ik} - \lambda_1 p_{uk}]
\\&q_{ik}:=q_{ik} + \alpha[(r_{ui}-{\sum_{k=1}}^k p_{uk}q_{ik})p_{uk} - \lambda_2 q_{ik}]
\end{split}
$$
由于P矩阵和Q矩阵是两个不同的矩阵，通常分别采取不同的正则参数，如$\lambda_1​$和$\lambda_2​$

#### 2.3.2.4. 代码实现
>```python
>import pandas as pd
>import numpy as np
>
>class LFM:
>    def __init__(self,alpha,reg_p,reg_q,number_latentFactors=10,number_epochs=10,solumns=['uid','iid','rating']):
>        self.alpha = alpha # 学习率
>        self.reg_p = reg_p # P 矩阵正则系数
>        self.reg_q = reg_q # Q 矩阵正则
>        self.number_latenFactors = number_latentFactors # 隐含类别的数量
>        self.number_epochs = number_epochs # 最大迭代次数
>        self.columns = columns
>    
>    def fit(self,dataset):
>        '''
>        fit dataset
>        :param dataset: uid, iid, rating
>        : return :
>        '''
>        self.dataset = pd.DataFrame(dataset)
>        self.users_ratings = self.dataset.groupby(self.columns[0]).agg([list])[[self.columns[1],self.columns[2]]]
>        self.item_ratings = self.dataset.groupby(self.columns[1]).agg([list])[[self.columns[0],self.columns[2]]]
>        self.global_mean = self.dataset[self.columns[2]].mean()
>        self.P,self.Q = self.sgd()
>
>    def __init_matrix__(self):
>        '''
>        初始化矩阵 P 和矩阵 Q
>        :return: P,Q
>        '''
>        # user-LF
>        P = dict(zip(self.users_ratings.index,np.random.rand(len(self.users_ratings),self.number_latenFactors).astype(np.float32)))
>        # item-LF
>        Q = dict(zip(self.item_ratings.index,np.random.rand(len(self.item_ratings),self.number_latenFactors).astype(np.float32)))
>        return P,Q
>    
>    def sgd(self):
>        '''
>        使用随机梯度下降法进行优化
>        '''
>        # 初始化矩阵
>        P,Q = self.__init_matrix__()
>        # 开始循环迭代
>        
>        for i in range(self.number_epochs):
>            print('iter %d'%d)
>            error_list = []
>            for uid,iid,r_ui in self.dataset.itertuples(index=False):
>                # 向量化计算
>                # 用户向量 user-LF P
>                v_pu = P[uid]
>                # 物品向量 item-LF Q
>                v_qi = Q[iid]
>                err = np.float32(r_ui - np.dot(v_pu,v_qi))
>
>                v_pu += self.alpha*(err*v_qi - self.reg_p*v_pu)
>                v_qi += self.alpha*(err*v_pu - self.reg_q*v_qi)
>
>                # 更新 P、Q 矩阵
>                P[uid] = v_pu
>                Q[iid] = v_qi
>
>                error_list.append(err**2)
>            print(np.sqrt(np.mean(error_list)))
>        return P,Q
>    
>    def predict(self,uid,iid):
>        if (uid not in self.users_ratings.index) or (iid not in self.item_ratings.index):
>            return self.global_mean
>        else:
>            return np.dot(self.P[uid],self.O[iid])
> ```


### 2.3.3. BiasSVD 算法
#### 2.3.3.1. 原理分析
- BiasSvd其实就是前面提到的 Funk SVD 矩阵分解基础上加上了偏置项 (baseline)。
- 利用BiasSvd预测用户对物品的评分，$k$表示隐含特征数量：

$$
\begin{split}
\hat {r}_{ui} &=\mu + b_u + b_i + \vec {p_{uk}}\cdot \vec {q_{ki}}
\\&=\mu + b_u + b_i + {\sum_{k=1}}^k p_{uk}q_{ik}
\end{split}
$$

#### 2.3.3.2. 损失函数

同样对于评分预测我们利用平方差来构建损失函数：
$$
\begin{split}
Cost &= \sum_{u,i\in R} (r_{ui}-\hat{r}_{ui})^2
\\&=\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i -{\sum_{k=1}}^k p_{uk}q_{ik})^2
\end{split}
$$
加入L2正则化：
$$
Cost = \sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})^2 + \lambda(\sum_U{b_u}^2+\sum_I{b_i}^2+\sum_U{p_{uk}}^2+\sum_I{q_{ik}}^2)
$$
对损失函数求偏导：
$$
\begin{split}
\cfrac {\partial}{\partial p_{uk}}Cost &= \cfrac {\partial}{\partial p_{uk}}[\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})^2 + \lambda(\sum_U{b_u}^2+\sum_I{b_i}^2+\sum_U{p_{uk}}^2+\sum_I{q_{ik}}^2)]
\\&=2\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})(-q_{ik}) + 2\lambda p_{uk}
\\\\
\cfrac {\partial}{\partial q_{ik}}Cost &= \cfrac {\partial}{\partial q_{ik}}[\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})^2 + \lambda(\sum_U{b_u}^2+\sum_I{b_i}^2+\sum_U{p_{uk}}^2+\sum_I{q_{ik}}^2)]
\\&=2\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})(-p_{uk}) + 2\lambda q_{ik}
\end{split}
$$

$$
\begin{split}
\cfrac {\partial}{\partial b_u}Cost &= \cfrac {\partial}{\partial b_u}[\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})^2 + \lambda(\sum_U{b_u}^2+\sum_I{b_i}^2+\sum_U{p_{uk}}^2+\sum_I{q_{ik}}^2)]
\\&=2\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})(-1) + 2\lambda b_u
\\\\
\cfrac {\partial}{\partial b_i}Cost &= \cfrac {\partial}{\partial b_i}[\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})^2 + \lambda(\sum_U{b_u}^2+\sum_I{b_i}^2+\sum_U{p_{uk}}^2+\sum_I{q_{ik}}^2)]
\\&=2\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})(-1) + 2\lambda b_i
\end{split}
$$



#### 2.3.3.3. 随机梯度下降法优化

梯度下降更新参数$p_{uk}$：
$$
\begin{split}
p_{uk}&:=p_{uk} - \alpha\cfrac {\partial}{\partial p_{uk}}Cost
\\&:=p_{uk}-\alpha [2\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})(-q_{ik}) + 2\lambda p_{uk}]
\\&:=p_{uk}+\alpha [\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})q_{ik} - \lambda p_{uk}]
\end{split}
$$
 同理：
$$
\begin{split}
q_{ik}&:=q_{ik} + \alpha[\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})p_{uk} - \lambda q_{ik}]
\end{split}
$$

$$
b_u:=b_u + \alpha[\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik}) - \lambda b_u]
$$

$$
b_i:=b_i + \alpha[\sum_{u,i\in R} (r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik}) - \lambda b_i]
$$

**随机梯度下降：**
$$
\begin{split}
&p_{uk}:=p_{uk}+\alpha [(r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})q_{ik} - \lambda_1 p_{uk}]
\\&q_{ik}:=q_{ik} + \alpha[(r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik})p_{uk} - \lambda_2 q_{ik}]
\end{split}
$$

$$
b_u:=b_u + \alpha[(r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik}) - \lambda_3 b_u]
$$

$$
b_i:=b_i + \alpha[(r_{ui}-\mu - b_u - b_i-{\sum_{k=1}}^k p_{uk}q_{ik}) - \lambda_4 b_i]
$$

由于P矩阵和Q矩阵是两个不同的矩阵，通常分别采取不同的正则参数，如$\lambda_1$和$\lambda_2$

#### 2.3.3.4. 代码实现
- 其余部分都一样，只是 sgd 迭代的时候有区别
>```python
>def sgd(self):
>    '''
>    使用随机梯度下降，优化结果
>    :return:
>    '''
>    P, Q = self._init_matrix()
>
>    # 初始化bu、bi的值，全部设为0
>    bu = dict(zip(self.users_ratings.index, np.zeros(len(self.users_ratings))))
>    bi = dict(zip(self.items_ratings.index, np.zeros(len(self.items_ratings))))
>
>    for i in range(self.number_epochs):
>        print("iter%d"%i)
>        error_list = []
>        for uid, iid, r_ui in self.dataset.itertuples(index=False):
>            v_pu = P[uid]
>            v_qi = Q[iid]
>            err = np.float32(r_ui - self.globalMean - bu[uid] - bi[iid] - np.dot(v_pu, v_qi))
>
>            v_pu += self.alpha * (err * v_qi - self.reg_p * v_pu)
>            v_qi += self.alpha * (err * v_pu - self.reg_q * v_qi)
>            
>            P[uid] = v_pu 
>            Q[iid] = v_qi
>            
>            bu[uid] += self.alpha * (err - self.reg_bu * bu[uid])
>            bi[iid] += self.alpha * (err - self.reg_bi * bi[iid])
>
>            error_list.append(err ** 2)
>        print(np.sqrt(np.mean(error_list)))
>
>    return P, Q, bu, bi
>```

## 2.4. 基于内容的推荐算法 (Content-Based)
- 基于内容的推荐方法比较直接，它以物品的内容描述信息为依据做出推荐
  - 本质上是基于物品和用户自身的特征或属性的直接分析和计算

### 2.4.1. 基于内容的推荐实现步骤
- **画像构建**：画像就是刻画物品或用户的特征，本质上就是给用户或物品贴标签。
  - **物品画像**
    - 分类信息、标题等
      - 例如：给电影《战狼2》贴标签

<div align=center>
<img src='./素材/战狼2.png' width=80%>
</div>

"动作"、"吴京"、"吴刚"、"张翰"、"大陆电影"、"国产"、"爱国"、"军事"等等一系列标签是不是都可以贴上

- **用户画像**
  - 喜好物品的类别、行为偏好
  - 活越程度
  - 风控维度
    - 例如已知用户的观影历史是："《战狼1》"、"《战狼2》"、"《建党伟业》"、"《建军大业》"、"《建国大业》"、"《红海行动》"、"《速度与激情1-8》"等，我们是不是就可以分析出该用户的一些兴趣特征如："爱国"、"战争"、"赛车"、"动作"、"军事"、"吴京"、"韩三平"等标签。
  - 此外，用户画像还可以包括用户的基本信息，例如：性别、年龄、消费档次...

#### 2.4.1.1. 标签从哪里来
- PGC(Professionally-generated Content) 物品画像-冷启动
  - 物品自带的属性（物品一产生就具备的属性）: 如电影标题、导演、演员、类型等
  - 服务提供方设定的属性 (服务提供方为物品附加的属性): 如短视频话题、微博话题 (平台拟定)
  - 其他渠道 : 爬虫
- UGC (User-generated Content) 冷启动问题
  - 用户在享手服务过程中提供的物品的属性 : 如用户评论内容、微博话题 (用户拟定)

根据PGC内容构建的物品画像可以解决冷启动问题

#### 2.4.1.2. 基于内容推荐的算法流程
- 基本流程
  - 根据 PGC/UGC 内容构建物品画像
  - 根据用户行为记录生成用户画像
  - 根据用户画像从物品中寻找最匹配的 TOP-N 物品进行推荐

#### 2.4.1.3. 物品冷启动处理
- 根据 PGC 内容构建物品画像
- 利用物品画像计算物品间两两相似情况 (画像-->词向量-->物品向量-->物品相似度)
- 为每个物品产生 TOP-N 最相似的物品进行相关推荐
  - 例如，与该商品相似的商品有哪些？与该文章相似文章有哪些？
  - 用户在浏览物品 A 时，就可以为其推荐与物品 A 相似的物品

### 2.4.2. 物品画像构建
- 物品画像构建步骤
  - 利用 tags.csv 中每部电影的标签作为电影的候选关键词
  - 利用 TF-IDF 计算每部电影的标签的 TF-IDF 值 (表示的是该标签的权重)，选取 TOP-N 个关键词作为电影画像标签
  - 将电影的分类词直接作为每部电影的画像标签
    - 例如：动作电影、爱情电影、恐怖电影等

- 其中 tag.csv 样例数据如下

|userId	|movieId	|tag	|timestamp|
|:-:|:-:|:-:|:-:|
|18	|4141	|Mark Waters	|1240597180|
|65	|208	|dark hero	|1368150078|
|65	|353	|dark hero	|1368150079|
|65	|521	|noir thriller	|1368149983|
|65	|592	|dark hero	|1368150078|
|...	|...	|...	|...|
|...	|...	|...	|...|
|65	|2726	|noir thriller	|1368149983|
|65	|2840	|jesus	|1368149925|
|65	|3052	|jesus	|1368149926|

#### 2.4.2.1. 基于 TF-IDF 的特征提取技术
- 物品画像的特征标签
  - 主要都是指的是如电影的导演、演员、图书的作者、出版社等结构化的数据
  - 对它们的特征向量计算比较简单，如直接 0、1 分类即可
- 隐含特征
  - 例如电影简介、影评、图书的摘要等文本类的数据，都是非结构化数据，他们也属于物品的特征标签，但是比较难以量化
  - 使用 NLP 技术去处理
##### 2.4.2.1.1. 算法原理
- TF-IDF算法基于一个这样的假设：若一个词语在目标文档中出现的频率高而在其他文档中出现的频率低，那么这个词语就可以用来区分出目标文档。这个假设需要掌握的有两点：
  - 在本文档出现的频率高；
  - 在其他文档出现的频率低。

- 假设文档集包含的文档数为 $N​$，文档集中包含关键词 $k_i​$ 的文档数为 $n_i​$，$f_{ij}​$ 表示关键词 $k_i​$ 在文档 $d_j​$ 中出现的次数，$f_{dj}​$ 表示文档 $d_j​$ 中出现的词语总数
  - 则 $k_i​$ 在文档dj中的词频 $TF_{ij}​$ 定义为：$TF_{ij}=f_{ij}/f_{dj}​$ 。
    - 这个数字通常会被正规化，以防止它偏向长的文件（指同一个词语在长文件里可能会比短文件有更高的词频，而不管该词语重要与否）。
  - $k_i​$ 的逆文档频率 $IDF_i=log(N/n_i)​$

由TF和IDF计算词语的权重为：$$w_{ij}=TF_{ij}*IDF_{i}=\frac {f_{ij}}{f_{dj}}*log\frac {N}{n_i}$$

##### 2.4.2.1.2. 数据集介绍
- movies.csv 样例数据
  - 这里 genres 一列实际是用 | 隔开，但由于 markdown 语法，用 \ 代替展示

|movieId	|title	|genres|
|:-:|:-:|:-:|
|1	|Toy Story (1995)	|Adventure\Animation\Children\Comedy\Fantasy}|
|2	|Jumanji (1995)	|Adventure\Children\Fantasy|
|3	|Grumpier Old Men (1995)	|Comedy\Romance|
|4	|Waiting to Exhale (1995)	|Comedy\Drama\Romance|
|5	|Father of the Bride Part II (1995)	|Comedy|
|6	|Heat (1995)	|Action\Crime\Thriller|
|7	|Sabrina (1995)	|Comedy\Romance|
|...	|...	|...|

- tags.csv 样例数据

|userId	|movieId	|tag	|timestamp|
|:-:|:-:|:-:|:-:|
|18	|4141	|Mark Waters	|1240597180|
|65	|208	|dark hero	|1368150078|
|65	|353	|dark hero	|1368150079|
|65	|521	|noir thriller	|1368149983|
|65	|592	|dark hero	|1368150078|
|...	|...	|...	|...|
|...	|...	|...	|...|
|65	|2726	|noir thriller	|1368149983|
|65	|2840	|jesus	|1368149925|
|65	|3052	|jesus	|1368149926|
##### 2.4.2.1.3. 代码实现
###### 2.4.2.1.3.1. 加载数据集
>```python
>import pandas as pd
>import numpy as np
>
>'''
>- 利用 tags.csv 中每部电影的标签作为电影的候选关键词
>- 利用 TF-IDF 计算每部电影的 tf-idf 权重，选择 TOP-N 个关键词作为电影画像标签
>- 并将电影的分类词直接作为每部电影的画像标签
>'''
>def get_movie_dataset():
>    # 加载基于所有电影的标签
>    # tags.csv 来自 ml-20m 数据集
>    _tags = pd.read_csv(r'E:\Jupyter_notebook\推荐系统\ml-20m\tags.csv',usecols=range(1,3)).dropna()
>    tags = _tags.groupby('movieId').agg(list)  # agg(list) 和 agg([list]) 本质是一样的
>    
>    # 加载电影列表数据集
>    movies = pd.read_csv(r'E:\Jupyter_notebook\推荐系统\ml-20m\movies.csv',index_col='movieId')
>    # 数据预处理，将类别分开
>    movies['genres'] = movies['genres'].apply(lambda x:x.split('|'))
>    # 为每部电影匹配上对应的标签
>    movies_index = movies.index.unique().intersection(tags.index.unique())
>    new_tags = tags.loc[movies_index,:]
>    ret = movies.join(new_tags) # pd.DataFrame 之间的 join，默认 left join, on 条件不给默认是 index-index
>
>    # 构建电影数据集。包括电影id、电影名称、类别、标签四个字段
>    # 如果电影没有标签数据，那么就替换为空列表
>    movie_dataset = pd.DataFrame(
>        map(
>            lambda x:(x[0],x[1],x[2],x[2]+x[3]) if x[3] is not np.nan else (x[0],x[1],x[2],[]) # tags.csv 文件携带的标签
>            ,ret.itertuples()
>        )
>        , columns=['movieId','title','genres','tags']
>    )
>    movie_dataset.set_index('movieId',inplace=True)
>    return movie_dataset
> ```

<div align=center>
<img src='./素材/movie_dataset.png'>
</div>

###### 2.4.2.1.3.2. 基于 TF-IDF 提取 TOP-N 关键词，构建电影画像
- gensim 简介
  - 参考资料：https://radimrehurek.com/gensim/auto_examples/index.html#documentation
  - 主要基于 NLP 的 Vector Space Model，基于词典进行构建
  - 基本概念
    - Corpus: 训练语料，Corpus通常是一个可迭代的对象（比如列表）。每一次迭代返回一个可用于表达文本对象的稀疏向量
  - **构建词典**
    - Dictinary类用于建立word<-->id之间的映射关系，把所有的单词取一个set()，并对set中的每一个单词分配一个id号的map
      ```python
      class gensim.corpora.dictionary.Dictionary(
           documents = None : # 若干个拆成单词的文档的集合，一般用list in list实现。list中包含的每个list都表示一个文档
           prune_at = 2000000 # 字典中的最大词条容量
      )
      ```
      - 构建词袋
      ```
      dct.doc2bow( # 转换为BOW格式：list of (tokenid,tokencount)
        document: 用于转换词条list。注意这里参数是 document，而Dictionary 中参数是documents
        allow_update = False : 是否直接更新所用字典
        return_missing = False : 是否返回新出现的（不在字典中的）词
       )

      输出结果
        (0,2),(1,2) , 表明在文档中id为0，1的词汇各出现了2次，至于其他词汇则没有出现
        return_missing = True时，输出list of (int , int),dict of (str, int)
      ```

- 代码实现
>```python
>from gensim.models import TfidfModel # 中英文通用
>from gensim.corpora import Dictionary
>
>def create_movie_profile(movie_data_set):
>    '''
>    计算电影画像，使用tf-idf，分析提取top-n关键词
>    :param movie_data_set: 前面基于 get_movie_dataset() 计算获得的数据集
>    :return keywords : 返回每部电影的 TOP-N 的关键词标签
>    '''
>    dataset = movie_data_set['tags'].values # 是一个 list in list 的结构
>    # 创建词典对象，Gensim 基于词典进行统计
>    dictionary = Dictionary(dataset)
>    corpus = [dictionary.doc2bow(line) for line in dataset] # 这里的 line 实际上是一个 list
>    # 得到的 corpus 是类似[[(0, 1),(1, 3),(2, 1)],[(3, 1),(4, 2),(5, 1)],[(6, 1),(7, 1),(8, 1)]] 的东西
>    # 传给 TF-IDF 模型训练
>    model = TfidfModel(corpus) 
>
>    movie_profile = {}
>    for i,mid in enumerate(movie_data_set.index):
>        # 根据每条数据返回向量
>        vector = model[corpus[i]] # 这是 gensim 模型的特有语法
>        # 按照 TF-IDF 值得到 TOP-N 的关键词
>        movie_tags = sorted(vector,key=lambda x:x[1],reverse=True)[:30]
>        # 得到每部电影对应的关键词名称及其权重
>        movie_profile[mid] = dict(map(lambda x:(dictionary[x[0]],x[1]),movie_tags))
>    
>    return movie_profile
> ```

###### 2.4.2.1.3.3. 完善画像关键词
- 将电影的类别 (genres) 标签，加入到构建的画像中去
  - 设置类别标签的权重为 1.0
>```python
>def create_movie_profile(movie_dataset):
>    '''
>    使用tfidf，分析提取topn关键词
>    :param movie_dataset:
>    :return:
>    '''
>    dataset = movie_dataset["tags"].values
>
>    from gensim.corpora import Dictionary
>    # 根据数据集建立词袋，并统计词频，将所有词放入一个词典，使用索引进行获取
>    dct = Dictionary(dataset)
>    # 根据将每条数据，返回对应的词索引和词频
>    corpus = [dct.doc2bow(line) for line in dataset]
>    # 训练TF-IDF模型，即计算TF-IDF值
>    model = TfidfModel(corpus)
>
>    _movie_profile = []
>    for i, data in enumerate(movie_dataset.itertuples()):
>        mid = data[0]
>        title = data[1]
>        genres = data[2]
>        vector = model[corpus[i]]
>        movie_tags = sorted(vector, key=lambda x: x[1], reverse=True)[:30]
>        topN_tags_weights = dict(map(lambda x: (dct[x[0]], x[1]), movie_tags))
>        # 将类别词的添加进去，并设置权重值为1.0
>        for g in genres:
>            topN_tags_weights[g] = 1.0
>        topN_tags = [i[0] for i in topN_tags_weights.items()]
>        _movie_profile.append((mid, title, topN_tags, topN_tags_weights))
>
>    movie_profile = pd.DataFrame(_movie_profile, columns=["movieId", "title", "profile", "weights"])
>    movie_profile.set_index("movieId", inplace=True)
>    return movie_profile
> ```

<div align=center>
<img src='./素材/movie_profile.png'>
</div>


###### 2.4.2.1.3.4. 倒排索引
- 为了根据指定关键词迅速匹配到对应的电影，因此需要对物品画像的标签词，建立**倒排索引**
  - 通常数据存储数据，都是以物品的ID作为索引，去提取物品的其他信息数据
  - 而倒排索引就是用物品的其他数据作为索引，去提取它们对应的物品的ID列表
- 倒排索引参考资料
  - https://blog.csdn.net/qq_43403025/article/details/114779166

>```python
>'''
>建立tag-物品的倒排索引
>'''
>
>def create_inverted_table(movie_profile): # movie_profile 来自 create_movie_profile() 计算结果
>    inverted_table = {}
>    for mid, weights in movie_profile["weights"].iteritems(): # movie_profile["weights"] 是一个 dict in dict
>        for tag, weight in weights.items():
>            #到inverted_table dict 用tag作为Key去取值，如果取不到就返回[]，如果取到了，则正常返回
>            _ = inverted_table.get(tag, [])
>            _.append((mid, weight))
>            inverted_table.setdefault(tag, _)
>    return inverted_table
>```

- 得到的结果如下
>```
>{'Pixar': [(1, 0.7094992907527269),
>  (2253, 0.08315837943158753),
>  (2294, 0.3819736353414594),
>   ...
>  (120468, 0.3628880299798657),
>  (120470, 0.6687258099541368),
>  (120474, 0.268984541548431)],
> 'animation': [(1, 0.38168191798267603),
>  (48, 0.13660377472904855),
>  (313, 0.2168867533568019),
>   ...
>  (594, 0.23495466621344424),
>  (595, 0.31458018555556194),
>  (596, 0.1180523692798225)]
>  ...
>}
>```

### 2.4.3. 用户画像构建 (基于物品标签建立)
- 首先介绍几个函数
- 1.collections.Counter()
>```python
>import collections
>collections.Counter(iterable=None, /, **kwds)
>'''
>Dict subclass for counting hashable items.  Sometimes called a bag
>or multiset.  Elements are stored as dictionary keys and their counts
>are stored as dictionary values.
>'''
>c = Counter('abcdeabcdabcaba')  # count elements from a string
>c.most_common(3)                # three most common elements
># 得到结果 [('a', 5), ('b', 4), ('c', 3)]
>```
- 2. reduce()
>```python
>from functools import reduce
>
>reduce(function, sequence[, initial]) -> value # 多个数据合成一个数据
>'''
>Apply a function of two arguments cumulatively to the items of a sequence,
>from left to right, so as to reduce the sequence to a single value.
>For example, reduce(lambda x, y: x+y, [1, 2, 3, 4, 5]) calculates
>((((1+2)+3)+4)+5).  If initial is present, it is placed before the items
>of the sequence in the calculation, and serves as a default when the
>sequence is empty.
>'''
>```

- 代码实现
>```python
>import collections
>from functools import reduce
>
>def create_user_profile():
>    watch_record = pd.read_csv(r'E:\Jupyter_notebook\推荐系统\ml-20m\ratings.csv',
>                        usecols=range(2),dtype={'userId':np.int32,'movieId':np.int32})
>    watch_record = watch_record.groupby('userId').agg(list)
>    # 创建物品画像
>    movie_dataset = get_movie_dataset()
>    movie_profile = create_movie_profile(movie_dataset)
>
>    user_profile = {}
>    for uid,mids in watch_record.itertuples():
>        record_movie_profile = movie_profile.loc[mids]
>        counter = collections.Counter(
>            reduce(lambda x,y:list(x)+list(y),record_movie_profile['profile'].values)
>        ) # Counter() 返回一个字典，统计里面每个电影标签的个数
>        
>        # 兴趣词
>        interest_words = counter.most_common(50) # tuple in list
>        maxcount = interest_words[0][1] # 最感兴趣的标签的词频
>        interest_words = [(w,round(c/maxcount,4)) for w,c in interest_words] # 感兴趣的标签和权重
>        user_profile[uid] = interest_words
>    
>    return user_profile
> ```

- 得到结果如下
>```
>{1: [('Adventure', 1.0),
>  ('Fantasy', 0.9452),
>  ('Action', 0.9315),
>    ...
>  ('fantasy world', 0.1644),
>  ('psychology', 0.1507),
>  ('violence', 0.1507)],
> 2: [('Sci-Fi', 1.0),
>  ('Drama', 0.8261),
>  ('Action', 0.8261),
>  ('Thriller', 0.8261),
>    ...
>  ('cult film', 0.3043),
>  ('war', 0.3043)]
>}
>```

### 2.4.4. 为用户产生 TOP-N 推荐结果

>```python
>def create_top_N_recomand(user_profile,inverted_table,N):
>    for uid, interest_words in user_profile.items():
>        result_table = {} # {电影id:[0.2,0.5,0.7]}
>        for interest_word, interest_weight in interest_words:
>            related_movies = inverted_table[interest_word]
>            for mid, related_weight in related_movies:
>                _ = result_table.get(mid, []) # 为什么用[]？因为一个电影可能对应多个关键词，根据不同的关键词，有不同的推荐结果
>                _.append(interest_weight)    # 只考虑用户的兴趣程度
>                # _.append(related_weight)    # 只考虑兴趣词与电影的关联程度
>                # _.append(interest_weight*related_weight)    # 二者都考虑
>                result_table.setdefault(mid, _)
>
>         rs_result = map(lambda x: (x[0], sum(x[1])), result_table.items()) # 针对电影所有权重求和
>         rs_result = sorted(rs_result, key=lambda x:x[1], reverse=True)[:N] # TOP-N 推荐结果
>         yield uid,rs_result
> ```

### 2.4.5. 流程总结
- 建立物品画像
  - 标签来源
    - 1. 用户打标签 tag
    - 2. 电影的分类值
  - 根据电影 id 把 tag 和分类值合并起来，求 TF-IDF
    - 根据TF-IDF结果为每一部电影筛选 TOP-N 关键词
    - 建立了 “电影id-关键词-关键词权重” 数据集
  - 建立倒排索引
    - 为了通过关键词标签找到电影
      - 遍历 “电影id-关键词-关键词权重” 数据集，读取每一个关键词，关键词作为 key，[(关键词对应电影id,tfidf权重)] 为 value 保存到 dict 中
- 建立用户画像
  - 看用户看过哪些电影（对哪些电影进行过评分）
    - 在电影的"电影id-关键词-关键词权重"数据集中赵大鹏电影对应的关键词
  - 把用户看过的所有关键词放在一起，统计每个关键词词频
  - 出现次数多的关键词作为用户的兴趣词
    - 兴趣词就是用户画像的关键词
- 根据用户兴趣此找到对应电影
  - 多个兴趣词可能对应一个电影
  - 对每一部电影对应的所有关键词权重求和
  - 选取 TOP-N 进行推荐

### 2.4.6. 物品冷启动问题
- 根据 PGC 内容构建物品画像
- 利用物品画像计算物品间两两相似情况 (画像-->词向量-->物品向量-->物品相似度)
- 为每个物品产生 TOP-N 最相似的物品进行相关推荐
  - 例如，与该商品相似的商品有哪些？与该文章相似文章有哪些？
  - 用户在浏览物品 A 时，就可以为其推荐与物品 A 相似的物品
#### 2.4.6.1. word2vec 原理简介
- 使用 word2vec 可以计算电影所有标签词之间的关系程度，可用于计算电影之间的相似度
- word2vec 原理
  - 详细可参考 [word2vec中的数学眼里](./素材/Word2vec中的数学原理详解.pdf)
- one-hot vector VS word vector
  - one-hot representation (词袋模型)
    - 用一个很长的向量来表示一个词，向量的长度为词典 D 的大小 N，向量的分量只有一个 1，其余全是 0。1 的位置对应该词在词典中的索引。
      - 但是这种词向量有一些缺点，容易造成维数灾难，又不能很好地刻画词与词之间地相似性
    - word2vec 是一种分布式词向量表示方法
      - 通过训练将某种语言中地每个词映射成一个固定长度地短向量 (相对于 on-hot 来说短)，所有地这些词向量构成词向量空间，每一个向量可视为该空间中的一个点，在空间中引入“距离”，就可以根据词之间的距离来判断它们之间的相似性。
##### 2.4.6.1.1. CBOW 和 SKip-Gram
- CBOW把一个词从词窗剔除。在CBOW下给定*n*词围绕着词*w*，word2vec预测一个句子中其中一个缺漏的词*c*，即以概率$p(c|w)$来表示。相反地，Skip-gram给定词窗中的文本，预测当前的词$p(w|c)​$。

- **Continuous Bag-of-Words(CBOW)** 连续词袋向量
  - 功能：通过上下文预测当前词出现的概率
  - 案例
    - 假设文本如下：“the florid <u>prose of</u> **the** <u>nineteenth century.</u>”
    - 想象有个滑动窗口，中间的词是关键词，两边为相等长度的文本来帮助分析。文本的长度为7，就得到了7个one-hot向量，作为神经网络的输入向量，训练目标是：最大化在给定前后文本情况下输出正确关键词的概率，比如给定("prose","of","nineteenth","century")的情况下，要最大化输出"the"的概率，用公式表示就是  P("the"|("prose","of","nineteenth","century"))
    - 特性
      - hidden layer只是将权重求和，传递到下一层，是线性的
- **Continuous Skip-gram**
  - 功能：根据当前词预测上下文
  - 原理分析
    - 和CBOW相反，则我们要求的概率就变为 P(Context(w)|w)
- 使用案例
>```
>class gensim.models.word2vec.Word2Vec(
>   sentences = None : # 类似list of list的格式，对于特别大的文本，尽量考虑流式处理
>   size = 100 ：# 词条向量的维度，数据量充足时，300/500的效果比较好
>   wiindow = 5 : # 上下文窗口大小
>   workers = 3 :# 同时运行的线程数，多核系统可明显加速计算
>)
>
>其余细节参数设定：
>   min_count = 5 低频词过滤阈值，低于该词频的不纳入模型
>   max_vocab_size = None 每1千万词条需要1G内存，必要时设定该参数以节约空间
>   sample = 0.01 负例采样的比例设定
>  negative=5 一般为 5-20，设为0时不进行负例采样
>   iter = 5: 模型在语料库上的迭代次数，该参数将被取消
>
>与神经网络模型有关的参数设定：
>   seed = 1, alpha = 0.025 , min_alpha = 0.0001 , sg=0,hs=0
>
>
>w2v模型的保存和使用
>  w2vmodel.dave(存盘路径及文件名称)
>  w2vmodel.load(存盘路径和文件名称)
>```


##### 2.4.6.1.2. 代码实现
>```python
>import gensim
>
>movie_dataset = get_movie_dataset()
>movie_profile = create_movie_profile(movie_dataset)
>sentences = list(movie_profile["tags"].values)
>
>model = gensim.models.Word2Vec(sentences, window=3, min_count=1, iter=20) # window 表示查看上下文词的数量
>
>while True:
>    words = input("words: ")  # action
>    ret = model.wv.most_similar(positive=[words], topn=10)
>    print(ret)
> ```
- 结果如下
>```
>words: action
>[('SequelTo:Casino Royale[2006]', 0.9469106197357178), 
> ('Asian ladies', 0.8987222909927368), 
> ('spy', 0.8983461856842041), 
> ('fighting', 0.8938730955123901), 
> ('Paradox', 0.8916416764259338), 
> ('car chase', 0.8855253458023071), 
> ('Jesse Ventura', 0.8810268044471741), 
> ('strong-minded characters', 0.8777558207511902), 
> ('Good action', 0.8750581741333008), 
> ('martial arts', 0.8700062036514282)]
>```

#### 2.4.6.2. Doc2Vec
- Doc2Vec是建立在Word2Vec上的，用于直接计算以文档为单位的文档向量，这里我们将一部电影的所有标签词，**作为整个文档**，这样可以计算出每部电影的向量，通过计算向量之间的距离，来判断用于计算电影之间的相似程度。
  - Doc2Vec 就是用一个向量来表示一篇文档
- 这样可以解决物品冷启动问题

>```python
>import gensim, logging
>from gensim.models.doc2vec import Doc2Vec, TaggedDocument
>from gensim.test.utils import get_tmpfile
>logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
>
>documents = [TaggedDocument(words, [movie_id]) for movie_id, words in movie_profile["profile"].iteritems()]
>
># 训练模型并保存
>model = Doc2Vec(documents, vector_size=100, window=3, min_count=1, workers=4, epochs=20)  # documents 只要是 iterable of list 即可
>fname = get_tmpfile("my_doc2vec_model")
>model.save(fname)
>
>
>words = movie_profile["profile"].loc[6]
>print(words)
>inferred_vector = model.infer_vector(words)
>sims = model.docvecs.most_similar([inferred_vector], topn=10)
>print(sims)
>```

- 结果如下
>```
>['Al Pacino', 
> 'Robert De Niro', 
> 'Michael Mann', 
> 'great acting', 
> 'realistic action', 
> 'Val Kilmer', 
> 'suspense', 
> 'tense', 
> 'Natalie Portman', 
> 'atmospheric', 
> 'bank robbery', 
> 'overrated', 
> 'philosophy', 
> 'crime', 
> 'long', 
> 'Los Angeles', 
> 'gunfight', 
> 'dialogue',
> 'visceral', 
> 'imdb top 250', 
> 'too long', 
> 'Bank Heist', 
> 'career criminal', 
> 'professionals', 'guns', 
> 'slow paced', 'Recommendz Top Pick', 'realistic', 'bleak', 'police', 'Action', 'Crime', 'Thriller']
>
>[(6, 0.9069437980651855),
> (6874, 0.8549489974975586), 
> (2959, 0.8445022702217102), 
> (48516, 0.8440171480178833), 
> (293, 0.8392611742019653), 
> (1089, 0.8354744911193848), 
> (112992, 0.8192870616912842), 
> (296, 0.8102130889892578), 
> (74545, 0.8033807277679443), 
> (908, 0.7992953062057495)]
> ```

- 有关 TaggedDoucment() 介绍
>```python
>class TaggedDocument(namedtuple('TaggedDocument', 'words tags')):
>    """Represents a document along with a tag, input document format for :class:`~gensim.models.doc2vec.Doc2Vec`.
>
>    A single document, made up of `words` (a list of unicode string tokens) and `tags` (a list of tokens).
>    Tags may be one or more unicode string tokens, but typical practice (which will also be the most memory-efficient)
>    is for the tags list to include a unique integer id as the only tag.
>
>    Replaces "sentence as a list of words" from :class:`gensim.models.word2vec.Word2Vec`.
>
>    """
>    def __str__(self):
>        """Human readable representation of the object's state, used for debugging.
>
>        Returns
>        -------
>        str
>           Human readable representation of the object's state (words and tags).
>
>        """
>        return '%s(%s, %s)' % (self.__class__.__name__, self.words, self.tags)
> ```

- 其中 namedtuple 来自 from collections import namedtuple
  - namedtuple 就是返回一个元组，但是元组里面的元素都有自己的名字
>```python
>def namedtuple(typename, field_names, *, rename=False, defaults=None, module=None):
>    """Returns a new subclass of tuple with named fields.
>
>    >>> Point = namedtuple('Point', ['x', 'y'])
>    >>> Point.__doc__                   # docstring for the new class
>    'Point(x, y)'
>    >>> p = Point(11, y=22)             # instantiate with positional args or keywords
>    >>> p[0] + p[1]                     # indexable like a plain tuple
>    33
>    >>> x, y = p                        # unpack like a regular tuple
>    >>> x, y
>    (11, 22)
>    >>> p.x + p.y                       # fields also accessible by name
>    33
>    >>> d = p._asdict()                 # convert to a dictionary
>    >>> d['x']
>    11
>    >>> Point(**d)                      # convert from a dictionary
>    Point(x=11, y=22)
>    >>> p._replace(x=100)               # _replace() is like str.replace() but targets named fields
>    Point(x=100, y=22)
>
>    """
>```



